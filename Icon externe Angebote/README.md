## :warning: Es darf keine kommerzielle Nutzung mit diesen SVG-Grafiken betrieben werden

Generiert mit [Android Material Icon Generator](https://android-material-icon-generator.bitdroid.de/) von Philipp Eichhorn
 (zum [GitHub Repository](https://github.com/Maddoc42/Android-Material-Icon-Generator/tree/master)) unter der Lizenz: [Creative Commons Attribution-Non-Commercial 3.0 License](https://creativecommons.org/licenses/by-nc/3.0/)

**Einstellungen:**
- Background color: #cc142c
- Background shape: Circle
- Shadow length: (Mindestlänge bis Iconrand)
- Shadow intensity: 0.3
- Shadow fading: 1
- Icon size: 5.3-5.6
