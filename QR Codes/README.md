URLs verkürzt mit [bit.ly](http://bit.ly/)  
QR-Codes generiert mit [goqr.me](http://goqr.me/de/)  

- App: [http://bit.ly/2UrQF4s](http://bit.ly/2UrQF4s) → [https://play.google.com/store/apps/details?id=de.hsgerlenbach.bapp](https://play.google.com/store/apps/details?id=de.hsgerlenbach.bapp)
- Website: [http://bit.ly/2Dij9rw](http://bit.ly/2Dij9rw) → [https://bapp.hsgerlenbach.de/
](https://bapp.hsgerlenbach.de/)
