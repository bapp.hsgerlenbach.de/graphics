## Offizielle Grafiken der Entwickler des Projektes „bapp“ und weiteren.

**Farbe:** `#cc142c` `rgb(204, 20, 44)` `hsl(352, 82%, 44%)`

**Symbolgrafiken:** [Material Icons](https://material.io/tools/icons/) (Google) und [Material Design Icons](https://materialdesignicons.com/) (featured by Google)